## [1.0.5](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/compare/v1.0.4...v1.0.5) (2021-11-29)


### Bug Fixes

* remove branches from config ([c03074c](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/c03074c81e395056b9f2295de0fd4f2ed86b412c))

## [1.0.4](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/compare/v1.0.3...v1.0.4) (2021-11-28)


### Bug Fixes

* adjust publish ([1c31da9](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/1c31da9eb53fd3d7d884c1bc9149ff49497124af))

## [1.0.3](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/compare/v1.0.2...v1.0.3) (2021-11-28)


### Bug Fixes

* remove private to package.json ([1d8d377](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/1d8d37761bad3727693720454eb7f8a1ebd91169))

## [1.0.2](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/compare/v1.0.1...v1.0.2) (2021-11-28)


### Bug Fixes

* set package as public ([83b7a5b](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/83b7a5b617de6099b1d93992a89946d9ff334c18))

## [1.0.1](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/compare/v1.0.0...v1.0.1) (2021-11-28)


### Bug Fixes

* cleanup ([10dfd2d](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/10dfd2dcf9977038d4c73b0e6771478122a1ac1b))

# 1.0.0 (2021-11-28)


### Bug Fixes

* build ([fefc4c5](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/fefc4c51e538e56808c04de22f7c4b26dd4da3ab))
* build ([ee11956](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/ee1195644c7a2427d924adda79b4eee175d554ee))
* deploy ([0de9b0a](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/0de9b0ad4994fd7b3a1dce985a898ff3e2ae33ff))
* fix npm publish ([bc44c54](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/bc44c5460b76acfc8a46df5e80dc726fc97e3ca0))
* publish pipeline - fix repo remote configuration ([40f3408](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/40f3408200e97e7cd2f1215cc320c77848c720de))
* try fix buiild ([6f81104](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/6f81104f9267aac2f866a34fcb96075aff57142e))
* try fix build ([643b879](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/643b8791e80106f72175f0681ac9d316ff1392ee))
* try fix build ([aae8f5c](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/aae8f5c20d94052b5cab478c7e0f22ea1a3c49c6))
* try fix deploy ([5937ae2](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/5937ae25ddc8e6a49e1e59319d5023b8d24224cd))

# 1.0.0 (2021-11-28)


### Bug Fixes

* build ([fefc4c5](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/fefc4c51e538e56808c04de22f7c4b26dd4da3ab))
* build ([ee11956](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/ee1195644c7a2427d924adda79b4eee175d554ee))
* deploy ([0de9b0a](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/0de9b0ad4994fd7b3a1dce985a898ff3e2ae33ff))
* publish pipeline - fix repo remote configuration ([40f3408](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/40f3408200e97e7cd2f1215cc320c77848c720de))
* try fix buiild ([6f81104](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/6f81104f9267aac2f866a34fcb96075aff57142e))
* try fix build ([643b879](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/643b8791e80106f72175f0681ac9d316ff1392ee))
* try fix build ([aae8f5c](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/aae8f5c20d94052b5cab478c7e0f22ea1a3c49c6))
* try fix deploy ([5937ae2](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/5937ae25ddc8e6a49e1e59319d5023b8d24224cd))

# 1.0.0 (2021-11-28)


### Bug Fixes

* build ([fefc4c5](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/fefc4c51e538e56808c04de22f7c4b26dd4da3ab))
* build ([ee11956](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/ee1195644c7a2427d924adda79b4eee175d554ee))
* deploy ([0de9b0a](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/0de9b0ad4994fd7b3a1dce985a898ff3e2ae33ff))
* publish pipeline - fix repo remote configuration ([40f3408](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/40f3408200e97e7cd2f1215cc320c77848c720de))
* try fix build ([643b879](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/643b8791e80106f72175f0681ac9d316ff1392ee))
* try fix build ([aae8f5c](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/aae8f5c20d94052b5cab478c7e0f22ea1a3c49c6))

# 1.0.0 (2021-11-28)


### Bug Fixes

* build ([fefc4c5](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/fefc4c51e538e56808c04de22f7c4b26dd4da3ab))
* build ([ee11956](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/ee1195644c7a2427d924adda79b4eee175d554ee))
* deploy ([0de9b0a](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/0de9b0ad4994fd7b3a1dce985a898ff3e2ae33ff))
* publish pipeline - fix repo remote configuration ([40f3408](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/40f3408200e97e7cd2f1215cc320c77848c720de))
* try fix build ([643b879](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/643b8791e80106f72175f0681ac9d316ff1392ee))
* try fix build ([aae8f5c](https://bitbucket.org/dcg-solutions/linx-digital-semantic-release-npm-github-config/commits/aae8f5c20d94052b5cab478c7e0f22ea1a3c49c6))
